<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p align="center" dir="auto">
<a target="_blank" rel="noopener noreferrer nofollow" href="https://user-images.githubusercontent.com/8291514/213727234-cda046d6-28c6-491a-b284-b86c5cede25d.png#gh-light-mode-only"><img src="https://user-images.githubusercontent.com/8291514/213727234-cda046d6-28c6-491a-b284-b86c5cede25d.png#gh-light-mode-only" style="max-width: 100%;"></a>
<a target="_blank" rel="noopener noreferrer nofollow" href="https://user-images.githubusercontent.com/8291514/213727225-56186826-bee8-43b5-9b15-86e839d89393.png#gh-dark-mode-only"><img src="https://user-images.githubusercontent.com/8291514/213727225-56186826-bee8-43b5-9b15-86e839d89393.png#gh-dark-mode-only" style="max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苏帕贝斯</font></font></h1><a id="user-content-supabase" class="anchor" aria-label="永久链接：Supabase" href="#supabase"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://supabase.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Supabase</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是 Firebase 的开源替代品。我们使用企业级开源工具构建 Firebase 的功能。</font></font></p>
<ul class="contains-task-list">
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">托管 Postgres 数据库。</font></font><a href="https://supabase.com/docs/guides/database" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">身份验证和授权。</font></font><a href="https://supabase.com/docs/guides/auth" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动生成的 API。
</font></font><ul class="contains-task-list">
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">REST。</font></font><a href="https://supabase.com/docs/guides/api" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GraphQL。</font></font><a href="https://supabase.com/docs/guides/graphql" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时订阅。</font></font><a href="https://supabase.com/docs/guides/realtime" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
</ul>
</li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">功能。
</font></font><ul class="contains-task-list">
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库函数。</font></font><a href="https://supabase.com/docs/guides/database/functions" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">边缘函数</font></font><a href="https://supabase.com/docs/guides/functions" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
</ul>
</li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件存储。</font></font><a href="https://supabase.com/docs/guides/storage" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AI + 矢量/嵌入工具包。</font></font><a href="https://supabase.com/docs/guides/ai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">仪表板</font></font></li>
</ul>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/supabase/supabase/master/apps/www/public/images/github/supabase-dashboard.png"><img src="https://raw.githubusercontent.com/supabase/supabase/master/apps/www/public/images/github/supabase-dashboard.png" alt="Supabase 仪表板" style="max-width: 100%;"></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关注此 repo 的“发布”以获取重大更新的通知。</font></font></p>
<p dir="auto"><kbd><animated-image data-catalyst=""><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/supabase/supabase/d5f7f413ab356dc1a92075cb3cee4e40a957d5b1/web/static/watch-repo.gif" data-target="animated-image.originalLink"><img src="https://raw.githubusercontent.com/supabase/supabase/d5f7f413ab356dc1a92075cb3cee4e40a957d5b1/web/static/watch-repo.gif" alt="Watch this repo" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
      <span class="AnimatedImagePlayer" data-target="animated-image.player" hidden="">
        <a data-target="animated-image.replacedLink" class="AnimatedImagePlayer-images" href="https://raw.githubusercontent.com/supabase/supabase/d5f7f413ab356dc1a92075cb3cee4e40a957d5b1/web/static/watch-repo.gif" target="_blank">
          
      
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></h2><a id="user-content-documentation" class="anchor" aria-label="永久链接：文档" href="#documentation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如需完整文档，请访问</font></font><a href="https://supabase.com/docs" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">supabase.com/docs</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要了解如何贡献，请访问</font></font><a href="/supabase/supabase/blob/master/DEVELOPERS.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">社区与支持</font></font></h2><a id="user-content-community--support" class="anchor" aria-label="固定链接：社区与支持" href="#community--support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/supabase/supabase/discussions"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">社区论坛</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。最适合：帮助构建、讨论数据库最佳实践。</font></font></li>
<li><a href="https://github.com/supabase/supabase/issues"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GitHub 问题</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。最适合：使用 Supabase 时遇到的错误和错误。</font></font></li>
<li><a href="https://supabase.com/docs/support#business-support" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">电子邮件支持</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。最适合：数据库或基础设施问题。</font></font></li>
<li><a href="https://discord.supabase.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Discord</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。最适合：分享您的应用程序并与社区一起闲逛。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">怎么运行的</font></font></h2><a id="user-content-how-it-works" class="anchor" aria-label="永久链接：工作原理" href="#how-it-works"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Supabase 是开源工具的组合。我们使用企业级开源产品构建 Firebase 的功能。如果存在具有 MIT、Apache 2 或同等开放许可证的工具和社区，我们将使用和支持该工具。如果不存在该工具，我们将自行构建和开源。Supabase 不是 Firebase 的 1 对 1 映射。我们的目标是让开发人员使用开源工具获得类似 Firebase 的开发体验。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">建筑学</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Supabase 是一个</font></font><a href="https://supabase.com/dashboard" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">托管平台</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。您可以注册并开始使用 Supabase，无需安装任何东西。您也可以</font></font><a href="https://supabase.com/docs/guides/hosting/overview" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自行托管</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并</font></font><a href="https://supabase.com/docs/guides/local-development" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在本地进行开发</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/supabase/supabase/blob/master/apps/docs/public/img/supabase-architecture.svg"><img src="/supabase/supabase/raw/master/apps/docs/public/img/supabase-architecture.svg" alt="建筑学" style="max-width: 100%;"></a></p>
<ul dir="auto">
<li><a href="https://www.postgresql.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Postgres</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一个对象关系数据库系统，拥有超过 30 年的活跃开发历史，在可靠性、功能稳健性和性能方面赢得了良好的声誉。</font></font></li>
<li><a href="https://github.com/supabase/realtime"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Realtime</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一个 Elixir 服务器，允许您使用 websockets 监听 PostgreSQL 的插入、更新和删除。Realtime 轮询 Postgres 的内置复制功能以查找数据库更改，将更改转换为 JSON，然后通过 websockets 将 JSON 广播给授权客户端。</font></font></li>
<li><a href="http://postgrest.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PostgREST</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一个 Web 服务器，可将您的 PostgreSQL 数据库直接转换为 RESTful API</font></font></li>
<li><a href="https://github.com/supabase/gotrue"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GoTrue</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一个基于 JWT 的 API，用于管理用户和发布 JWT 令牌。</font></font></li>
<li><a href="https://github.com/supabase/storage-api"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供了一个 RESTful 接口来管理存储在 S3 中的文件，并使用 Postgres 来管理权限。</font></font></li>
<li><a href="http://github.com/supabase/pg_graphql/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">pg_graphql 是</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一个 PostgreSQL 扩展，可公开 GraphQL API</font></font></li>
<li><a href="https://github.com/supabase/postgres-meta"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgres-meta</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一个用于管理 Postgres 的 RESTful API，允许您获取表、添加角色、运行查询等。</font></font></li>
<li><a href="https://github.com/Kong/kong"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kong</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一个云原生 API 网关。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">客户端库</font></font></h4><a id="user-content-client-libraries" class="anchor" aria-label="永久链接：客户端库" href="#client-libraries"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们对客户端库的方法是模块化的。每个子库都是单个外部系统的独立实现。这是我们支持现有工具的方法之一。</font></font></p>
<table>
  <tbody><tr>
    <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言</font></font></th>
    <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">客户</font></font></th>
    <th colspan="5"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">功能客户端 (捆绑在 Supabase 客户端中)</font></font></th>
  </tr>
  
  <tr>
    <th></th>
    <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苏帕贝斯</font></font></th>
    <th><a href="https://github.com/postgrest/postgrest"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PostgREST</font></font></a></th>
    <th><a href="https://github.com/supabase/gotrue"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">真心</font></font></a></th>
    <th><a href="https://github.com/supabase/realtime"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">即时的</font></font></a></th>
    <th><a href="https://github.com/supabase/storage-api"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贮存</font></font></a></th>
    <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">功能</font></font></th>
  </tr>
  
  
  
  <tr><th colspan="7"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⚡️官方⚡️</font></font></th>
  
  </tr><tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">JavaScript（TypeScript）</font></font></td>
    <td><a href="https://github.com/supabase/supabase-js"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">supabase-js</font></font></a></td>
    <td><a href="https://github.com/supabase/postgrest-js"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-js</font></font></a></td>
    <td><a href="https://github.com/supabase/gotrue-js"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gotrue-js</font></font></a></td>
    <td><a href="https://github.com/supabase/realtime-js"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时 js</font></font></a></td>
    <td><a href="https://github.com/supabase/storage-js"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-js</font></font></a></td>
    <td><a href="https://github.com/supabase/functions-js"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">函数-js</font></font></a></td>
  </tr>
    <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">扑</font></font></td>
    <td><a href="https://github.com/supabase/supabase-flutter"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">supabase 颤动</font></font></a></td>
    <td><a href="https://github.com/supabase/postgrest-dart"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-dart</font></font></a></td>
    <td><a href="https://github.com/supabase/gotrue-dart"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gotrue-dart</font></font></a></td>
    <td><a href="https://github.com/supabase/realtime-dart"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时飞镖</font></font></a></td>
    <td><a href="https://github.com/supabase/storage-dart"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-dart</font></font></a></td>
    <td><a href="https://github.com/supabase/functions-dart"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">函数-dart</font></font></a></td>
  </tr>
  <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迅速</font></font></td>
    <td><a href="https://github.com/supabase/supabase-swift"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">supabase-swift</font></font></a></td>
    <td><a href="https://github.com/supabase/supabase-swift/tree/main/Sources/PostgREST"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-swift</font></font></a></td>
    <td><a href="https://github.com/supabase/supabase-swift/tree/main/Sources/Auth"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">auth-swift</font></font></a></td>
    <td><a href="https://github.com/supabase/supabase-swift/tree/main/Sources/Realtime"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时swift</font></font></a></td>
    <td><a href="https://github.com/supabase/supabase-swift/tree/main/Sources/Storage"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-swift</font></font></a></td>
    <td><a href="https://github.com/supabase/supabase-swift/tree/main/Sources/Functions"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">函数-swift</font></font></a></td>
  </tr>
  
  <tr><th colspan="7"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">💚 社区 💚</font></font></th>
  
  </tr><tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">C＃</font></font></td>
    <td><a href="https://github.com/supabase-community/supabase-csharp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">supabase-csharp</font></font></a></td>
    <td><a href="https://github.com/supabase-community/postgrest-csharp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-csharp</font></font></a></td>
    <td><a href="https://github.com/supabase-community/gotrue-csharp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gotrue-csharp</font></font></a></td>
    <td><a href="https://github.com/supabase-community/realtime-csharp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时-csharp</font></font></a></td>
    <td><a href="https://github.com/supabase-community/storage-csharp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-csharp</font></font></a></td>
    <td><a href="https://github.com/supabase-community/functions-csharp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">函数-csharp</font></font></a></td>
  </tr>
  <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">去</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><a href="https://github.com/supabase-community/postgrest-go"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-go</font></font></a></td>
    <td><a href="https://github.com/supabase-community/gotrue-go"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gotrue-go</font></font></a></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><a href="https://github.com/supabase-community/storage-go"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-go</font></font></a></td>
    <td><a href="https://github.com/supabase-community/functions-go"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">函数-go</font></font></a></td>
  </tr>
  <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Java</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><a href="https://github.com/supabase-community/gotrue-java"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gotrue-java</font></font></a></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><a href="https://github.com/supabase-community/storage-java"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-java</font></font></a></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
  </tr>
  <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">科特林</font></font></td>
    <td><a href="https://github.com/supabase-community/supabase-kt"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苏巴贝斯</font></font></a></td>
    <td><a href="https://github.com/supabase-community/supabase-kt/tree/master/Postgrest"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-kt</font></font></a></td>
    <td><a href="https://github.com/supabase-community/supabase-kt/tree/master/GoTrue"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gotrue-kt</font></font></a></td>
    <td><a href="https://github.com/supabase-community/supabase-kt/tree/master/Realtime"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时-kt</font></font></a></td>
    <td><a href="https://github.com/supabase-community/supabase-kt/tree/master/Storage"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-kt</font></font></a></td>
    <td><a href="https://github.com/supabase-community/supabase-kt/tree/master/Functions"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">功能-kt</font></font></a></td>
  </tr>
  <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python</font></font></td>
    <td><a href="https://github.com/supabase-community/supabase-py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超碱基-py</font></font></a></td>
    <td><a href="https://github.com/supabase-community/postgrest-py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-py</font></font></a></td>
    <td><a href="https://github.com/supabase-community/gotrue-py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gotrue-py</font></font></a></td>
    <td><a href="https://github.com/supabase-community/realtime-py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时py</font></font></a></td>
    <td><a href="https://github.com/supabase-community/storage-py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-py</font></font></a></td>
    <td><a href="https://github.com/supabase-community/functions-py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">函数-py</font></font></a></td>
  </tr>
  <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">红宝石</font></font></td>
    <td><a href="https://github.com/supabase-community/supabase-rb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">supabase-rb</font></font></a></td>
    <td><a href="https://github.com/supabase-community/postgrest-rb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-rb</font></font></a></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
  </tr>
  <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">锈</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><a href="https://github.com/supabase-community/postgrest-rs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-rs</font></font></a></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
  </tr>
  <tr>
    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Godot 引擎 (GDScript)</font></font></td>
    <td><a href="https://github.com/supabase-community/godot-engine.supabase"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">supabase-gdscript</font></font></a></td>
    <td><a href="https://github.com/supabase-community/postgrest-gdscript"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">postgrest-gdscript</font></font></a></td>
    <td><a href="https://github.com/supabase-community/gotrue-gdscript"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gotrue-gdscript</font></font></a></td>
    <td><a href="https://github.com/supabase-community/realtime-gdscript"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时gdscript</font></font></a></td>
    <td><a href="https://github.com/supabase-community/storage-gdscript"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储-gdscript</font></font></a></td>
    <td><a href="https://github.com/supabase-community/functions-gdscript"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">函数-gdscript</font></font></a></td>
  </tr>
  
</tbody></table>


<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">徽章</font></font></h2><a id="user-content-badges" class="anchor" aria-label="永久链接：徽章" href="#badges"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/supabase/supabase/blob/master/apps/www/public/badge-made-with-supabase.svg"><img src="/supabase/supabase/raw/master/apps/www/public/badge-made-with-supabase.svg" alt="采用 Supabase 制造" style="max-width: 100%;"></a></p>
<div class="highlight highlight-text-md notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-s">[</span><span class="pl-s">![</span>Made with Supabase<span class="pl-s">]</span><span class="pl-s">(</span><span class="pl-corl">https://supabase.com/badge-made-with-supabase.svg</span><span class="pl-s">)]</span><span class="pl-s">(</span><span class="pl-corl">https://supabase.com</span><span class="pl-s">)</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="[![Made with Supabase](https://supabase.com/badge-made-with-supabase.svg)](https://supabase.com)" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="highlight highlight-text-html-basic notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-kos">&lt;</span><span class="pl-ent">a</span> <span class="pl-c1">href</span>="<span class="pl-s">https://supabase.com</span>"<span class="pl-kos">&gt;</span>
  <span class="pl-kos">&lt;</span><span class="pl-ent">img</span>
    <span class="pl-c1">width</span>="<span class="pl-s">168</span>"
    <span class="pl-c1">height</span>="<span class="pl-s">30</span>"
    <span class="pl-c1">src</span>="<span class="pl-s">https://supabase.com/badge-made-with-supabase.svg</span>"
    <span class="pl-c1">alt</span>="<span class="pl-s">Made with Supabase</span>"
  /&gt;
<span class="pl-kos">&lt;/</span><span class="pl-ent">a</span><span class="pl-kos">&gt;</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="<a href=&quot;https://supabase.com&quot;>
  <img
    width=&quot;168&quot;
    height=&quot;30&quot;
    src=&quot;https://supabase.com/badge-made-with-supabase.svg&quot;
    alt=&quot;Made with Supabase&quot;
  />
</a>" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/supabase/supabase/blob/master/apps/www/public/badge-made-with-supabase-dark.svg"><img src="/supabase/supabase/raw/master/apps/www/public/badge-made-with-supabase-dark.svg" alt="采用 Supabase（深色）制成" style="max-width: 100%;"></a></p>
<div class="highlight highlight-text-md notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-s">[</span><span class="pl-s">![</span>Made with Supabase<span class="pl-s">]</span><span class="pl-s">(</span><span class="pl-corl">https://supabase.com/badge-made-with-supabase-dark.svg</span><span class="pl-s">)]</span><span class="pl-s">(</span><span class="pl-corl">https://supabase.com</span><span class="pl-s">)</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="[![Made with Supabase](https://supabase.com/badge-made-with-supabase-dark.svg)](https://supabase.com)" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="highlight highlight-text-html-basic notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-kos">&lt;</span><span class="pl-ent">a</span> <span class="pl-c1">href</span>="<span class="pl-s">https://supabase.com</span>"<span class="pl-kos">&gt;</span>
  <span class="pl-kos">&lt;</span><span class="pl-ent">img</span>
    <span class="pl-c1">width</span>="<span class="pl-s">168</span>"
    <span class="pl-c1">height</span>="<span class="pl-s">30</span>"
    <span class="pl-c1">src</span>="<span class="pl-s">https://supabase.com/badge-made-with-supabase-dark.svg</span>"
    <span class="pl-c1">alt</span>="<span class="pl-s">Made with Supabase</span>"
  /&gt;
<span class="pl-kos">&lt;/</span><span class="pl-ent">a</span><span class="pl-kos">&gt;</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="<a href=&quot;https://supabase.com&quot;>
  <img
    width=&quot;168&quot;
    height=&quot;30&quot;
    src=&quot;https://supabase.com/badge-made-with-supabase-dark.svg&quot;
    alt=&quot;Made with Supabase&quot;
  />
</a>" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">翻译</font></font></h2><a id="user-content-translations" class="anchor" aria-label="固定链接：翻译" href="#translations"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="/supabase/supabase/blob/master/i18n/README.ar.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿拉伯语 |阿拉伯联合酋长国</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.sq.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿尔巴尼亚语/Shqip</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.bn.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">孟加拉语 / বাংলা</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.bg.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保加利亚语 / Български</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.ca.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加泰罗尼亚语 / Català</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.cs.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">捷克语 / čeština</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.da.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">丹麦语/丹斯克语</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.nl.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">荷兰语 / 荷兰语</font></font></a></li>
<li><a href="https://github.com/supabase/supabase"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">英语</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.et.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">爱沙尼亚语/eesti keel</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.fi.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">芬兰语 / Suomalainen</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.fr.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法语 / Français</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.de.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">德语 / Deutsch</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.el.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">希腊语 / Ελληνικά</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.gu.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">古吉拉特语 / ગુજરાતી</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.he.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">希伯来语 / עברית</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.hi.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">印地语 / हिंदी</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.hu.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">匈牙利/马扎尔语</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.ne.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">尼泊尔语 / नेपाली</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.id.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">印度尼西亚语 / 印尼语</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.it.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Italiano/意大利语</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.jp.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">日语 / 日本語</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.ko.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">韩语 / 한국어</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.lt.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">立陶宛语 / lietuvių</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.lv.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">拉脱维亚语 / latviski</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.ms.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">马来语 / 马来文</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.nb.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">挪威语（博克马尔语）/挪威语（博克马尔语）</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.fa.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">波斯语 / فارسی</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.pl.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">波兰语 / 波兰语</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.pt.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">葡萄牙语 / Português</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.pt-br.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">葡萄牙语（巴西）/Português Brasileiro</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.ro.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">罗马尼亚语 / Română</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.ru.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">俄语 / Pусский</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.sr.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">塞尔维亚语/Srpski</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.si.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">僧伽罗语 / සිංහල</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.sk.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯洛伐克语 / slovenský</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.sl.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯洛文尼亚语 / Slovenščina</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.es.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">西班牙语 / Español</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.zh-cn.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">簡體中文</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.sv.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瑞典语 / Svenska</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.th.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">泰语 / ไทย</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.zh-tw.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">繁體中文</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.tr.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">土耳其语 / Türkçe</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.uk.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">乌克兰语 / Українська</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/README.vi-vn.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">越南语 / Tiếng Việt</font></font></a></li>
<li><a href="/supabase/supabase/blob/master/i18n/languages.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">翻译列表</font></font></a> </li>
</ul>
</article></div>
